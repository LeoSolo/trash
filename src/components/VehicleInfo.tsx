import * as React from 'react'
import { Select } from './Select'
import { SetVehicleAction } from '../actions/vehicle'
import { Uploader } from './Uploader'

interface VehicleInfoProps {
	data: any,
	setVehicle: (
		carBrand: string,
		carModel: string,
		carYear: string,
		carProfile: string,
		carPhoto1: string,
		carPhoto2: string,
		carPhoto3: string,
		carPhoto4: string,
		carPhoto5: string
	) => SetVehicleAction
}

interface VehicleInfoStates {
	carBrandSelect: string,
	carModelSelect: string,
	carYearSelect: string,
	carProfileSelect: string,
	carPhoto1Bg: string,
	carPhoto2Bg: string,
	carPhoto3Bg: string,
	carPhoto4Bg: string,
	carPhoto5Bg: string
}

export class VehicleInfo extends React.Component<VehicleInfoProps, VehicleInfoStates> {
	constructor(props: any) {
		super(props)
		this.state = {
			carBrandSelect: this.props.data.carBrand,
			carModelSelect: this.props.data.carModel,
			carYearSelect: this.props.data.carYear,
			carProfileSelect: this.props.data.carProfile,
			carPhoto1Bg: this.props.data.carPhoto1,
			carPhoto2Bg: this.props.data.carPhoto2,
			carPhoto3Bg: this.props.data.carPhoto3,
			carPhoto4Bg: this.props.data.carPhoto4,
			carPhoto5Bg: this.props.data.carPhoto5
		}
	}
	onChange = (e: React.ChangeEvent<HTMLInputElement> & Event) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}
	onChangeImg = (base64Img: string, e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			[e.target.name]: base64Img
		})
	}
	onSubmit = () => {
		const { setVehicle } = this.props
		setVehicle(
			this.state.carBrandSelect,
			this.state.carModelSelect,
			this.state.carYearSelect,
			this.state.carProfileSelect,
			this.state.carPhoto1Bg,
			this.state.carPhoto2Bg,
			this.state.carPhoto3Bg,
			this.state.carPhoto4Bg,
			this.state.carPhoto5Bg
		)
	}
    render() {
		let template = (
			<React.Fragment>
				<div className="carPhoto">
					<Uploader
						onChangeImg={this.onChangeImg}
						img={this.state.carPhoto1Bg}
						name="carPhoto1Bg"
					/>
				</div>
				{
					!!this.state.carPhoto1Bg &&
					<div className="carPhoto">
						<Uploader
							onChangeImg={this.onChangeImg}
							img={this.state.carPhoto2Bg}
							name="carPhoto2Bg"
						/>
					</div>
				}
				{
					!!this.state.carPhoto2Bg &&
					<div className="carPhoto">
						<Uploader
							onChangeImg={this.onChangeImg}
							img={this.state.carPhoto3Bg}
							name="carPhoto3Bg"
						/>
					</div>
				}
				{
					!!this.state.carPhoto3Bg &&
					<div className="carPhoto">
						<Uploader
							onChangeImg={this.onChangeImg}
							img={this.state.carPhoto4Bg}
							name="carPhoto4Bg"
						/>
					</div>
				}
				{
					!!this.state.carPhoto4Bg &&
					<div className="carPhoto">
						<Uploader
							onChangeImg={this.onChangeImg}
							img={this.state.carPhoto5Bg}
							name="carPhoto5Bg"
						/>
					</div>
				}
			</React.Fragment>
		)

        return (
            <div>
                <div className="vehicleImg" />
                <p>We trust you have a safe vehicle that is presentable when you arrive at a customer's site.
                    Also the type of vehicle will help us guide you to loads where a trailer
	                or a dumptruck is needed.</p>
	            <div className="inputsContainer">
		            <label>Brand:
			            <Select
				            mock="carsBrands"
				            value={this.state.carBrandSelect}
				            name="carBrandSelect"
				            onChange={this.onChange}
			            />
		            </label>
		            <label>Year:
			            <Select
				            mock="carsYears"
				            value={this.state.carYearSelect}
				            name="carYearSelect"
				            onChange={this.onChange}
			            />
		            </label>
		            <label>Model:
			            <Select
				            mock="carsModels"
				            value={this.state.carModelSelect}
				            name="carModelSelect"
				            onChange={this.onChange}
			            />
		            </label>
		            <label>Profile:
			            <Select
				            mock="carsProfiles"
				            value={this.state.carProfileSelect}
				            name="carProfileSelect"
				            onChange={this.onChange}
			            />
		            </label>
	            </div>
	            <div className="imgsContainer">
		            Please add photos
		            {template}
	            </div>
                <div className="btn updateBtn" onClick={this.onSubmit}>Update</div>
            </div>
        )
    }
}

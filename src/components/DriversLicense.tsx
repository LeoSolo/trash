import * as React from 'react'
import { Select } from './Select'
import { SetLicenseAction } from '../actions/driversLicense'
import { Uploader } from './Uploader'
import DatePicker from 'react-datepicker'
import * as moment from 'moment'

interface DriversLicenseProps {
    data: any,
    setLicense: (
        licenseState: string,
        licenseNumber: string,
        licenseExpires: string,
        licenseFrontImg: string,
        licenseBackImg: string
    ) => SetLicenseAction
}

interface DriversLicenseStates {
	licenseStateSelect: string,
	licenseNumberInput: string,
	licenseExpiresInput: string,
	licenseFrontImgBg: string,
	licenseBackImgBg: string,
	startDate: any
}

export class DriversLicense extends React.Component<DriversLicenseProps, DriversLicenseStates> {
    constructor(props: any) {
        super(props)
        this.state = {
	        licenseStateSelect: this.props.data.licenseState,
	        licenseNumberInput: this.props.data.licenseNumber,
	        licenseExpiresInput: this.props.data.licenseExpires,
	        licenseFrontImgBg: this.props.data.licenseFrontImg,
	        licenseBackImgBg: this.props.data.licenseBackImg,
	        startDate: moment()
        }
    }
    onChange = (e: React.ChangeEvent<HTMLInputElement> & Event) => {
	    this.setState({
		    [e.target.name]: e.target.value
	    })
    }
	onChangeImg = (base64Img: string, e: React.ChangeEvent<HTMLInputElement>) => {
	    this.setState({
		    [e.target.name]: base64Img
	    })
    }
    onSubmit = () => {
	    const { setLicense } = this.props
	    setLicense(
		    this.state.licenseStateSelect,
		    this.state.licenseNumberInput,
		    this.state.licenseExpiresInput,
		    this.state.licenseFrontImgBg,
		    this.state.licenseBackImgBg
	    )
    }
	handleChange = (date: any) => {
		this.setState({
			startDate: date,
			licenseExpiresInput: date.format('MM/DD/YYYY')
		})
	}
    render() {
	    let startDay = this.state.licenseExpiresInput ? this.state.licenseExpiresInput : this.state.startDate
        return (
            <div>
                <div className="licenseImg" />
                <p>Please provide your drivers license information. This is needed to verify your age
                    ans the bank will need it when you provide your banking information. After all, you
                    would like to see all hard earned money deposited into your account, right?</p>
	            <div className="inputsContainer">
                    <label>
                        State:
	                    <Select
		                    name="licenseStateSelect"
		                    mock="state"
		                    value={this.state.licenseStateSelect}
		                    onChange={this.onChange}
	                    />
                    </label>
		            <label>
                        DL Number:
			            <input
				            type="text"
				            name="licenseNumberInput"
				            placeholder="0000-0000-00000-00"
				            value={this.state.licenseNumberInput}
				            onChange={this.onChange}
			            />
                    </label>
		            <label>
                        Expires:
			            <DatePicker
				            selected={moment(startDay)}
				            onChange={this.handleChange}
				            showYearDropdown={true}
				            scrollableYearDropdown={true}
				            yearDropdownItemNumber={50}
				            readOnly={true}
				            maxDate={moment()}
				            name="licenseExpiresInput"
			            />
                    </label>
                </div>
	            <div className="imgsContainer">
		            <div className="frontLicense">
			            License Front Side
			            <Uploader
				            onChangeImg={this.onChangeImg}
				            img={this.state.licenseFrontImgBg}
				            name="licenseFrontImgBg"
			            />
		            </div>
		            <div className="backLicense">
			            License Back Side
			            <Uploader
				            onChangeImg={this.onChangeImg}
				            img={this.state.licenseBackImgBg}
				            name="licenseBackImgBg"
			            />
		            </div>
	            </div>
                <div className="btn updateBtn" onClick={this.onSubmit}>Update</div>
            </div>
        )
    }
}

import * as React from 'react'

interface UploaderProps {
	onChangeImg: Function,
	img: string,
	name: string
}

interface UploaderStates {
	imagesPreviewUrl: string,
	file: string
}

export class Uploader extends React.Component<UploaderProps, UploaderStates> {
	constructor(props: any) {
		super(props)
		this.state = {
			file: '',
			imagesPreviewUrl: ''
		}
	}
	componentWillMount() {
		this.setState({
			imagesPreviewUrl: this.props.img
		})
	}
	_handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		// TODO: do something with -> this.state.file
		console.log('handle uploading-', this.state.file)
	}
	_handleImageChange = (e: any) => {
		e.preventDefault()
		e.persist()

		let reader = new FileReader()
		let file = e.target.files[0]

		reader.onloadend = () => {
			this.setState({
				file: file,
				imagesPreviewUrl: reader.result
			})

			this.props.onChangeImg(this.state.imagesPreviewUrl, e)
		}

		reader.readAsDataURL(file)
	}
	render() {
		let imageUploadContainerStyle: Object = {
			backgroundImage: 'url(' + this.state.imagesPreviewUrl + ')',
			backgroundPosition: 'center center',
			backgroundRepeat: 'no-repeat',
			backgroundSize: 'contain'
		}
		return (
			<div className="imageUploadContainer" style={imageUploadContainerStyle} >
				<div className="imageUploadBtn">
					<form onSubmit={this._handleSubmit}>
						<input
							className="fileInput"
							type="file"
							onChange={this._handleImageChange}
							name={this.props.name}
						/>
					</form>
				</div>
			</div>
		)
	}
}

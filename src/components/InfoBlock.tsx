import * as React from 'react'

export class InfoBlock extends React.Component {
    render() {
        return (
            <div className="infoContainer">
                <p>Make money on your own terms, work full or part-time with no office and no boss, work where and when you want</p>
                <ul className="rulesList">
                    <li>
                        Advantages of being a Go Trashy hauler
                        <span>- How easy it is, be your own boss, make money, flexible work hours, etc.</span>
                    </li>
                    <li>
                        What it takes to become a hauler. Prerequisites:
                        <span>- 21 years or older</span>
                        <span>- Valid drivers license</span>
                        <span>- Vehicle Standards.</span>
                        <span>- DMV and Background Check.</span>
                        <span>- Commercial Insurance coverage for hauler.</span>
                    </li>
                    <li>
                        Desired Market Service Area
                        <span>- The hauler validates on a map that there are available slots in the interested MSA.</span>
                    </li>
                    <li>
                        Fees incurred for the background check (and ongoing credentialing). Further discussion needed on whether this will be refunded after ‘n’ number of completed jobs.
                    </li>
                    <li>
                        Complete an over the telephone conversation with a Go Trashy customer success representative.
                    </li>
                </ul>
            </div>
        )
    }
}
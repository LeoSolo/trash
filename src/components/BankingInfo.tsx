import * as React from 'react'
import * as classnames from 'classnames'
import { SetBankInfoAction } from '../actions/bankInfo'

interface BankingInfoProps {
	data: any,
	setBankInfo: (
		bankInfoCard: string,
		bankRoutingNumber: string,
		bankAccountNumber: string,
		bankSocialSecurityNumber: string,
		debitCardInfo: string,
		expiredCardDate: string,
		cardCCVNumber: string
	) => SetBankInfoAction
}

interface BankingInfoStates {
	bankChooseInput: string,
	bankRoutingNumberInput: string,
	bankAccountNumberInput: string,
	bankSocialSecurityNumberInput: string,
	debitCardInfoInput: string,
	expiredCardDateInput: string,
	cardCCVNumberInput: string
}

export class BankingInfo extends React.Component<BankingInfoProps, BankingInfoStates> {
	constructor(props: any) {
		super(props)
		this.state = {
			bankChooseInput: this.props.data.bankInfoCard,
			bankRoutingNumberInput: this.props.data.bankRoutingNumber,
			bankAccountNumberInput: this.props.data.bankAccountNumber,
			bankSocialSecurityNumberInput: this.props.data.bankSocialSecurityNumber,
			debitCardInfoInput: this.props.data.debitCardInfo,
			expiredCardDateInput: this.props.data.expiredCardDate,
			cardCCVNumberInput: this.props.data.cardCCVNumber
		}
	}
	onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}
	onSubmit = () => {
		const { setBankInfo } = this.props

		switch (this.state.bankChooseInput) {
			case 'bankAccount':
				setBankInfo(
					this.state.bankChooseInput,
					this.state.bankRoutingNumberInput,
					this.state.bankAccountNumberInput,
					this.state.bankSocialSecurityNumberInput,
					'',
					'',
					''
				)
				break
			case 'debitCard':
				setBankInfo(
					this.state.bankChooseInput,
					'',
					'',
					'',
					this.state.debitCardInfoInput,
					this.state.expiredCardDateInput,
					this.state.cardCCVNumberInput
				)
				break
		}
	}
	onChangeCheckbox = (e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}
	render() {
		return (
			<div>
				<div className="bankingImg"/>
				<p>This is the last information that we need for you to get paid. Provide a valid bank checking
					account card (not credit card) or your banking information. Note that Go Trashy does not store this
					information and all monetary transactions are 100% gurenateed to be secure.</p>

				<div
					className={classnames('card bankAccountInfo', { active: this.state.bankChooseInput === 'bankAccount' })}
				>
					<label>
						<input
							type="radio"
							name="bankChooseInput"
							onChange={this.onChangeCheckbox}
							value="bankAccount"
							checked={this.state.bankChooseInput === 'bankAccount'}
						/>
						Bank Account Information
					</label>

					<label>
						Routing Number:
						<input
							name="bankRoutingNumberInput"
							type="text"
							placeholder="00000000"
							onChange={this.onChange}
							value={this.state.bankRoutingNumberInput}
						/>
					</label>
					<label>
						Account Number:
						<input
							name="bankAccountNumberInput"
							type="text"
							placeholder="0000000000000000"
							onChange={this.onChange}
							value={this.state.bankAccountNumberInput}
						/>
					</label>
					<label>
						Social Security Number:
						<input
							name="bankSocialSecurityNumberInput"
							type="text"
							placeholder="123-45-6789"
							onChange={this.onChange}
							value={this.state.bankSocialSecurityNumberInput}
						/>
					</label>

					<span className="bankAnswer error">
                        The provided bank account information is not valid. Please try again.
                    </span>
				</div>

				<div
					className={classnames('card debitCardInfo', { active: this.state.bankChooseInput === 'debitCard' })}
				>
					<label>
						<input
							type="radio"
							name="bankChooseInput"
							onChange={this.onChangeCheckbox}
							value="debitCard"
							checked={this.state.bankChooseInput === 'debitCard'}
						/>
						Debit Card Information
					</label>

					<label>
						Debit Card Information:
						<input
							name="debitCardInfoInput"
							type="text"
							placeholder="0000000000000000"
							onChange={this.onChange}
							value={this.state.debitCardInfoInput}
						/>
					</label>
					<label>
						Expiration Date:
						<input
							name="expiredCardDateInput"
							type="text"
							placeholder="01/01/2023"
							onChange={this.onChange}
							value={this.state.expiredCardDateInput}
						/>
					</label>
					<label>
						CCV Number:
						<input
							name="cardCCVNumberInput"
							type="text"
							placeholder="XXX"
							onChange={this.onChange}
							value={this.state.cardCCVNumberInput}
						/>
					</label>

					<span className="bankAnswer" />
				</div>
				<div className="btn updateBtn" onClick={this.onSubmit}>Update</div>
			</div>
		)
	}
}

import * as React from 'react'
import { mockingOptions } from '../mocks/mock'

export interface SelectProps {
	mock: string
	value: string
	name: string
	onChange(e: Event): void
}

export class Select extends React.Component<SelectProps> {
	constructor (props: any) {
		super(props)
	}
	onChange = (e: React.ChangeEvent<HTMLSelectElement> & Event) => {
		this.props.onChange(e)
	}
	render() {
		let template = mockingOptions[this.props.mock].map((item: string): Object => {
			return (
				<option key={item}>{item}</option>
			)
		})

		return (
			<select name={this.props.name} value={this.props.value} onChange={this.onChange} >
				{template}
			</select>
		)
	}
}

import * as React from 'react'
import DatePicker from 'react-datepicker'
import * as moment from 'moment'
import { SetPersonalInfoAction } from '../actions/personalInfo'
import { Select } from './Select'
import { Uploader } from './Uploader'

interface PersonalInfoProps {
	data: any,
	setPersonalInfo: (
		company: string,
		whoseHauler: string,
		firstName: string,
		lastName: string,
		email: string,
		phone: string,
		street: string,
		city: string,
		userState: string,
		zipCode: string,
		birthday: string,
		userAvatar: string
	) => SetPersonalInfoAction
}

interface PersonalInfoStates {
	companyNameInput: string,
	whoseHaulerSelect: string,
	firstNameInput: string,
	lastNameInput: string,
	emailInput: string,
	phoneInput: string,
	streetInput: string,
	cityInput: string,
	stateSelect: string,
	zipCodeInput: string,
	startDate: any,
	birthdayInput: string
	userAvatarBg: string
}

export class PersonalInfo extends React.Component<PersonalInfoProps, PersonalInfoStates> {
	constructor(props: any) {
		super(props)
		this.state = {
			companyNameInput: this.props.data.company,
			whoseHaulerSelect: this.props.data.whoseHauler,
			firstNameInput: this.props.data.firstName,
			lastNameInput: this.props.data.lastName,
			emailInput: this.props.data.email,
			phoneInput: this.props.data.phone,
			streetInput: this.props.data.street,
			cityInput: this.props.data.city,
			stateSelect: this.props.data.userState,
			zipCodeInput: this.props.data.zipCode,
			birthdayInput: this.props.data.birthday,
			userAvatarBg: this.props.data.userAvatar,
			startDate: moment()
		}
	}

	handleChange = (date: any) => {
		this.setState({
			startDate: date,
			birthdayInput: date.format('MM/DD/YYYY')
		})
	}
	onChange = (e: React.ChangeEvent<HTMLInputElement> & Event) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}
	onChangeImg = (base64Img: string, e: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			[e.target.name]: base64Img
		})
	}
	onSubmit = () => {
		const { setPersonalInfo } = this.props
		setPersonalInfo(
			this.state.companyNameInput,
			this.state.whoseHaulerSelect,
			this.state.firstNameInput,
			this.state.lastNameInput,
			this.state.emailInput,
			this.state.phoneInput,
			this.state.streetInput,
			this.state.cityInput,
			this.state.stateSelect,
			this.state.zipCodeInput,
			this.state.birthdayInput,
			this.state.userAvatarBg
		)
	}
	render() {
		let startDay = this.state.birthdayInput ? this.state.birthdayInput : this.state.startDate
		return (
			<div>
				<div className="avatarImg">
					Choose avatar
					<Uploader
						onChangeImg={this.onChangeImg}
						img={this.state.userAvatarBg}
						name="userAvatarBg"
					/>
				</div>
				<p>Please provide your personal information.
					If you happenend to operate as a company enter your company name also.
					The geographic area that you will be assigned to is based on your Zip code.
					If you would like to operate in a larger geographic area, you will have the opportunity to submit
					a request once your application was approved</p>
				<div className="inputsContainer">
					<div className="title">Your Contact Information</div>
					<input
						type="text"
						name="companyNameInput"
						placeholder="Company Name"
						onChange={this.onChange}
						value={this.state.companyNameInput}
					/>
					<Select
						name="whoseHaulerSelect"
						mock="whoseHauler"
						value={this.state.whoseHaulerSelect}
						onChange={this.onChange}
					/>
					<input
						type="text"
						name="firstNameInput"
						placeholder="First Name"
						onChange={this.onChange}
						value={this.state.firstNameInput}
					/>
					<input
						type="text"
						name="lastNameInput"
						placeholder="Last Name"
						onChange={this.onChange}
						value={this.state.lastNameInput}
					/>
					<input
						type="text"
						name="emailInput"
						placeholder="e-mail address"
						onChange={this.onChange}
						value={this.state.emailInput}
					/>
					<input
						type="text"
						name="phoneInput"
						placeholder="Telephone number"
						onChange={this.onChange}
						value={this.state.phoneInput}
					/>
					<input
						type="text"
						name="streetInput"
						placeholder="Street"
						onChange={this.onChange}
						value={this.state.streetInput}
					/>
					<input
						type="text"
						name="cityInput"
						placeholder="City"
						onChange={this.onChange}
						value={this.state.cityInput}
					/>
					<Select
						name="stateSelect"
						mock="state"
						value={this.state.stateSelect}
						onChange={this.onChange}
					/>
					<input
						type="text"
						name="zipCodeInput"
						placeholder="Zip Code"
						onChange={this.onChange}
						value={this.state.zipCodeInput}
					/>

					<div className="dateInputContainer">
						DOB:
						<DatePicker
							selected={moment(startDay)}
							onChange={this.handleChange}
							showYearDropdown={true}
							scrollableYearDropdown={true}
							yearDropdownItemNumber={50}
							readOnly={true}
							maxDate={moment()}
							name="birthdayInput"
						/>
					</div>
				</div>
				<div className="btn updateBtn" onClick={this.onSubmit}>Update</div>
			</div>
		)
	}
}

import * as React from 'react'

interface AppOverviewProps {
	changeTab: Function
}

export class AppOverview extends React.Component<AppOverviewProps> {
    constructor(props: any) {
        super(props)
    }
    onUpdate = (e: React.MouseEvent<HTMLDivElement>) => {
        this.props.changeTab(Number(e.target.dataset.index))
    }
    render() {
        return (
            <div>
                <div className="haulerImg"/>
                <p>This is an overview of your Go Trashy hauler application process.
                    If it from this page that you can find out your application status and update it in case something
                    is requested
                    from you by a Go Trashy hauler success manager.</p>
                <div className="statusContainer">
                    <p>Your Status:</p>
                    <p>Hello Bob. Please provide the requested information and submit it to Go Trashy. Somebody
                        will contact you withing two business days. Background check, Insutance check and banking
                        information check will be performed after
                        the above listed data was verified.</p>
                </div>
                <div className="overviewListContainer">
                    <ul>
                        <li>
                            <div className="statusImg checked"/>
                            <div className="title">Personal Information</div>
                            <div data-index="1" className="btn fixBtn" onClick={this.onUpdate}>Update</div>
                        </li>
                        <li>
                            <div className="statusImg checked"/>
                            <div className="title">Drivers License Information Pending</div>
                            <div data-index="2" className="btn fixBtn" onClick={this.onUpdate}>Update</div>
                        </li>
                        <li>
                            <div className="statusImg checked"/>
                            <div className="title">Vehicle Information Pending</div>
                            <div data-index="3" className="btn fixBtn" onClick={this.onUpdate}>Update</div>
                        </li>
	                    <li>
		                    <div className="statusImg"/>
		                    <div className="title">Banking Information Data Pending</div>
		                    <div data-index="4" className="btn fixBtn" onClick={this.onUpdate}>Update</div>
	                    </li>
                        <li>
                            <div className="statusImg waiting"/>
                            <div className="title">Background Check Pending</div>
	                        <div className="btn fixBtn" style={{ opacity: 0, pointerEvent: 'none', cursor: 'auto' }}>Update</div>
                        </li>
                        <li>
                            <div className="statusImg waiting"/>
                            <div className="title">Bussines Insurance Check Pending</div>
	                        <div className="btn fixBtn" style={{ opacity: 0, pointerEvent: 'none', cursor: 'auto' }}>Update</div>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

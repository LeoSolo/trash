import * as React from 'react'
import { RouterState, RouterAction, routerActions } from 'react-router-redux'
import { connect, Dispatch } from 'react-redux'
import { StoreState } from '../reducers'
import * as constants from '../constants/hauler-constants'

import { bindActionCreators } from 'redux'
import { DataState } from '../reducers/data'

interface AuthorizationProps {
	data: DataState
    routing: RouterState
	routePush: (path: string) => RouterAction
}

export class Authorization extends React.Component<AuthorizationProps> {
	constructor(props: any) {
		super(props)
	}
	route = () => {
        this.props.routePush('/App')
	}
	componentDidMount() {
		console.log(window.location.href)
	}
	logIn = (event: React.MouseEvent<HTMLDivElement>) => {
        let provider = event.target.dataset.provider

        window.location.href =
	        constants.HAULER_API.AUTH_URL + 'oauth2/authorize?response_type=code&client_id=' +
	        constants.HAULER_API.CLIENT_ID + '&redirect_uri=' +
	        constants.HAULER_API.REDIRECT_URI_LOGIN + '&scope=openid+email+profile+aws.cognito.signin.user.admin&identity_provider=' +
	        provider
    }
    render() {
        return (
            <div>
	            <div className="shadowBlock">
		            Loading...
		            <div className="preloader" />
	            </div>

                <p>Authorization</p>

                <div className="authContainer">
                    <div className="btn facebookBtn" data-provider="Facebook" onClick={this.logIn} >
                        <div />
                        <div>Facebook</div>
                    </div>
                    <div className="btn amazonBtn" data-provider="Amazon" onClick={this.logIn} >
                        <div />
                        <div>Amazon</div>
                    </div>
                    <div className="btn googleBtn" data-provider="Google" onClick={this.logIn} >
                        <div />
                        <div>Google</div>
                    </div>
                </div>

                <div className="btn nextBtn navBtn" onClick={this.route}>Next</div>

	            <div className="btn navBtn">New account</div>

            </div>
        )
    }
}

function mapStateToProps(state: StoreState) {
    return {
        routing: state.routing
    }
}

function mapDispatchToProps(dispatch: Dispatch<RouterAction>) {
	return bindActionCreators(
		{
			routePush: routerActions.push
		},
		dispatch
	)
}

export const AuthorizationContainer = connect(
    mapStateToProps,
	mapDispatchToProps
)(Authorization)

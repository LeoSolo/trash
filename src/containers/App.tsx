import * as React from 'react'
import { connect, Dispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RouterState, RouterAction, routerActions } from 'react-router-redux'

import { StoreState } from '../reducers'

import { Link } from 'react-router-dom'
import { DataState } from '../reducers/data'
import { setFirstForm, SetFirstFormAction, FirstFormActions } from '../actions/firstForm'

interface AppProps {
	routing: RouterState
    data: DataState
    routePush: (path: string) => RouterAction
    setFirstForm: (
        firstName: string,
        lastName: string,
        email: string,
        phone: string,
        company: string,
        zipCode: string,
        hearAbout: string,
        ownTruck: boolean,
        ownTrailer: boolean,
        bgCheck: boolean
    ) => SetFirstFormAction
}

interface AppStates {
    firstNameInput: string,
    lastNameInput: string,
    emailInput: string,
    phoneInput: string,
    companyInput: string,
    zipCodeInput: string,
    hearAboutSelect: string,
    ownTruckCheckbox: boolean,
    ownTrailerCheckbox: boolean,
    bgCheckCheckbox: boolean
}

export class App extends React.Component<AppProps, AppStates> {
    constructor(props: any) {
        super(props)
        this.state = {
            firstNameInput: this.props.data.firstName,
            lastNameInput: this.props.data.lastName,
            emailInput: this.props.data.email,
            phoneInput: this.props.data.phone,
            companyInput: this.props.data.company,
            zipCodeInput: this.props.data.zipCode,
            hearAboutSelect: this.props.data.hearAbout,
            ownTruckCheckbox: this.props.data.ownTruck,
            ownTrailerCheckbox: this.props.data.ownTrailer,
            bgCheckCheckbox: this.props.data.bgCheck
        }
    }
    onChange = (e: React.ChangeEvent<HTMLInputElement> & React.ChangeEvent<HTMLSelectElement>) => {
        if (e.target.type === 'checkbox') {
            this.setState({
                [e.target.name]: e.target.checked
            })
        } else {
            this.setState({
                [e.target.name]: e.target.value
            })
        }
    }
    onSubmit = () => {
        const { setFirstForm } = this.props
        setFirstForm(
            this.state.firstNameInput,
            this.state.lastNameInput,
            this.state.emailInput,
            this.state.phoneInput,
            this.state.companyInput,
            this.state.zipCodeInput,
            this.state.hearAboutSelect,
            this.state.ownTruckCheckbox,
            this.state.ownTrailerCheckbox,
            this.state.bgCheckCheckbox
        )

	   this.sendData()
    }
    sendData = () => {
	    fetch('https://89gw7m0jc2.execute-api.us-west-2.amazonaws.com/dev/haulers', {
		    method: 'post',
		    headers: {},
		    body: JSON.stringify({
			    firstName : this.state.firstNameInput,
			    lastName: this.state.lastNameInput,
			    primaryPhone : this.state.phoneInput,
			    mailingAddress : this.state.emailInput,
			    companyId : this.state.companyInput,
			    zip : this.state.zipCodeInput
		    })
	    })
		    .then(function(data) {
			    console.log('Request succeeded with JSON response', data.json())
		    })
		    .catch(function(error) {
			    console.log('Request failed', error)
		    })
    }
	render() {
		return (
			<div>
                <h1>Get Started</h1>
                <p>Please start the application with completing this form. Based on it we can determine if there are hauler openings in your desired geographic area.</p>

                <div className="inputsContainer">
                    <input type="text" name="firstNameInput" placeholder="First Name" value={this.state.firstNameInput} onChange={this.onChange} />
                    <input type="text" name="lastNameInput" placeholder="Last Name" value={this.state.lastNameInput} onChange={this.onChange} />
                    <input type="text" name="emailInput" placeholder="e-mail address" value={this.state.emailInput} onChange={this.onChange} />
                    <input type="text" name="phoneInput" placeholder="Telephone Number" value={this.state.phoneInput} onChange={this.onChange} />
                    <input type="text" name="companyInput" placeholder="Company Name" value={this.state.companyInput} onChange={this.onChange} />
                    <input type="text" name="zipCodeInput" placeholder="What is your ZIP code?" value={this.state.zipCodeInput} onChange={this.onChange} />
                    <select name="hearAboutSelect" required={true} onChange={this.onChange} defaultValue={this.state.hearAboutSelect}>
                        <option value="" disabled={true} selected={true} hidden={true}>How did you hear about Go Trashy?</option>
                        <option value="hear from dad">From dad</option>
                        <option value="hear from mom">From mom</option>
                        <option value="hear from friends">From friends</option>
                        <option value="hear from TV">From TV</option>
                    </select>
                    <label><input type="checkbox" name="ownTruckCheckbox" onChange={this.onChange} checked={this.state.ownTruckCheckbox} />I have my own truck</label>
                    <label><input type="checkbox" name="ownTrailerCheckbox" onChange={this.onChange} checked={this.state.ownTrailerCheckbox} />I have my own trailer</label>
                    <label><input type="checkbox" name="bgCheckCheckbox" onChange={this.onChange} checked={this.state.bgCheckCheckbox} />I am willing to submit to a background check</label>
                </div>

				{/*<Link className="btn backBtn navBtn" to="/" onClick={this.onSubmit}>Back</Link>*/}
				{/*<Link className="btn nextBtn navBtn" to="/Overview" onClick={this.onSubmit}>Next</Link>*/}
				{/*<Link className="btn checkStatusBtn" to="#" onClick={this.onSubmit}>Check my application status</Link>*/}

				<div className="btn nextBtn navBtn" onClick={this.onSubmit}>Next</div>

			</div>
		)
	}
}

function mapStateToProps(state: StoreState) {
	return {
        data: state.data,
		routing: state.routing
	}
}

function mapDispatchToProps(dispatch: Dispatch<RouterAction | FirstFormActions>) {
	return bindActionCreators(
		{
			setFirstForm
		},
		dispatch
	)
}

export const AppContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(App)

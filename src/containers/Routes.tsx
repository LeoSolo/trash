import * as React from 'react'
import { Route } from 'react-router'
import { Switch } from 'react-router-dom'

import { AppContainer } from './App'
import { AuthorizationContainer } from './Authorization'
import { OverviewContainer } from './Overview'
import { Header } from '../components/Header'
import { InfoBlock } from '../components/InfoBlock'
import { LoginContainer } from './Login'
import { BindAccountnContainer } from './BindAccount'

export class Routes extends React.Component {
    render() {
        return (
			<React.Fragment>
                <Header />
                <div className="mainContainer">
                    <Switch>
                        <Route
                            exact={true}
                            path="/"
                            component={AuthorizationContainer}
                        />
	                    <Route
		                    exact={true}
		                    path="/Login"
		                    component={LoginContainer}
	                    />
                        <Route
                            exact={true}
                            path="/App"
                            component={AppContainer}
                        />
	                    <Route
		                    exact={true}
		                    path="/BindAccount"
		                    component={BindAccountnContainer}
	                    />
                        <Route
                            exact={true}
                            path="/Overview"
                            component={OverviewContainer}
                        />
                    </Switch>
                </div>
                <InfoBlock />
			</React.Fragment>
        )
    }
}

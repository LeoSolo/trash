import * as React from 'react'
import { connect, Dispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RouterState, RouterAction, routerActions } from 'react-router-redux'
import { StoreState } from '../reducers'
import * as constants from '../constants/hauler-constants'
import axios from 'axios'
import * as queryString from 'querystring'

interface LoginProps {
	routing: RouterState,
	routePush: (path: string) => RouterAction
}

export class Login extends React.Component<LoginProps> {
	constructor(props: any) {
		super(props)
	}
	componentDidMount() {
		// setTimeout(() => {
		// 	this.props.routePush('/App')    // routing
		// },4000)

		if (window.location.href.split('code=')[1]) {
			this.exchangingAuthorizationCodeForTokens(window.location.href.split('code=')[1])
		} else {
			console.log('User not found')
		}
	}
    exchangingAuthorizationCodeForTokens(code: string) {
        if (code) {
            let data = {
                grant_type: 'authorization_code',
                client_id: constants.HAULER_API.CLIENT_ID,
                code: code,
                redirect_uri: constants.HAULER_API.REDIRECT_URI_LOGIN
            }
            axios.post(constants.HAULER_API.AUTH_URL + '/oauth2/token', queryString.stringify(data), {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
            ).then(response => {
                console.error('+++ AXIOS exchangingAuthorizationCodeForTokens response: ', response)
	            console.log(response.data)

                // this.props.userAuthActions.updateUserAuth(response.data)
                // userAuthService.storeUserAuth(response.data)

                // console.error('+++ LOGIN +++ this.props.userAuth: ', this.props.userAuth);

                // this.props.history.push(`/platformUsers`);
                // this.props.history.push(`/`)
            })
        }
    }
	render() {
		return (
			<div>
				<div className="loginMessage">
					Loading...
					<div className="preloader" />
				</div>
			</div>
		)
	}
}

function mapStateToProps(state: StoreState) {
	return {
		routing: state.routing
	}
}

function mapDispatchToProps(dispatch: Dispatch<RouterAction>) {
	return bindActionCreators(
		{
			routePush: routerActions.push
		},
		dispatch
	)
}

export const LoginContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Login)

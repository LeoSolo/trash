import * as React from 'react'

export class Thanks extends React.Component {
	constructor(props: any) {
		super(props)
	}
	render() {
		return (
			<div>
				<p>We are sorry to let you know that the maximum allowable number of haulers are already operating in
					this geographic area. We will let you can determine at that time whether you are still interested.</p>
				<p>Thanks you again for applying.</p>
			</div>
		)
	}
}

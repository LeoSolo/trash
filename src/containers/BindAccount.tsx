import * as React from 'react'
import { RouterState, RouterAction, routerActions } from 'react-router-redux'
import { connect, Dispatch } from 'react-redux'
import { StoreState } from '../reducers'
// import { Link } from 'react-router-dom'
import * as constants from '../constants/hauler-constants'
import FacebookAuth from 'react-facebook-auth'
import { GoogleLogin } from 'react-google-login'

import { bindActionCreators } from 'redux'
import { DataState } from '../reducers/data'

interface BindAccountProps {
	data: DataState
	routing: RouterState
	routePush: (path: string) => RouterAction
}

export class BindAccount extends React.Component<BindAccountProps> {
	constructor(props: any) {
		super(props)
	}
	route = () => {
		this.props.routePush('/Overview')
	}
	componentDidMount() {
		console.log(window.location.href)
	}
	facebookAuth = response => {    // TODO можно будет объединить
		console.log(response)
	}
	googleAuth = response => {
		console.log(response)
	}
	render() {
		const MyFacebookButton = ({ onClick }) => (
		   <button onClick={onClick}>
		    Login with facebook
		   </button>
		)
		return (
			<div>
				<div className="shadowBlock">
					Loading...
					<div className="preloader" />
				</div>

				<p>Congratulations! We are pleased to let you know that there are hauler openings in your desired Zip Codes areas.
					Let's start with creating a Go Trashy account. You can authenticate with either Google, Amazon or a Facebook account.</p>

				<FacebookAuth
					appId="1600926940020250"
					callback={this.facebookAuth}
					component={MyFacebookButton}
				/>
				<br />
				<br />
				<br />
				<GoogleLogin
					clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
					buttonText="Login with Google"
					onSuccess={this.googleAuth}
					onFailure={this.googleAuth}
				/>

				<div className="btn nextBtn navBtn" onClick={this.route}>Next</div>

			</div>
		)
	}
}

function mapStateToProps(state: StoreState) {
	return {
		routing: state.routing
	}
}

function mapDispatchToProps(dispatch: Dispatch<RouterAction>) {
	return bindActionCreators(
		{
			routePush: routerActions.push
		},
		dispatch
	)
}

export const BindAccountnContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(BindAccount)

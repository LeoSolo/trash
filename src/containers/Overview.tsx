import * as React from 'react'
import { connect, Dispatch } from 'react-redux'
import { StoreState } from '../reducers'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import { bindActionCreators } from 'redux'
import { DataState } from '../reducers/data'

import { AppOverview } from '../components/AppOverview'
import { PersonalInfo } from '../components/PersonalInfo'
import { DriversLicense } from '../components/DriversLicense'
import { VehicleInfo } from '../components/VehicleInfo'
import { BankingInfo } from '../components/BankingInfo'

import { setPersonalInfo, SetPersonalInfoAction, PersonalInfoActions } from '../actions/personalInfo'
import { setLicense, SetLicenseAction, LicenseActions } from '../actions/driversLicense'
import { setVehicle, SetVehicleAction, VehicleActions } from '../actions/vehicle'
import { setBankInfo, SetBankInfoAction, BankInfoActions } from '../actions/bankInfo'

interface OverviewProps {
    data: DataState
	setPersonalInfo: (
		company: string,
		whoseHauler: string,
		firstName: string,
		lastName: string,
		email: string,
		phone: string,
		street: string,
		city: string,
		userState: string,
		zipCode: string
	) => SetPersonalInfoAction
	setLicense: (
		licenseState: string,
		licenseNumber: string,
		licenseExpires: string
	) => SetLicenseAction
	setVehicle: (
		carBrand: string,
		carModel: string,
		carYear: string,
		carProfile: string
	) => SetVehicleAction
	setBankInfo: (
		bankRoutingNumber: string,
		bankAccountNumber: string,
		bankSocialSecurityNumber: string,
		debitCardInfo: string,
		expiredCardDate: string,
		cardCCVNumber: string
	) => SetBankInfoAction
}

interface OverviewStates {
    tabIndex: number
}

export class Overview extends React.Component<OverviewProps, OverviewStates> {
    constructor(props: any) {
        super(props)
        this.state = {
            tabIndex: 0
        }
    }
    changeTab = (index: number) => {
	    this.setState({ tabIndex: index })
    }
    onTabSelect = (tabIndex: number) => {
	    this.setState({ tabIndex })
    }
    render() {
        return (
            <Tabs selectedIndex={this.state.tabIndex} onSelect={this.onTabSelect}>
                <TabList>
                    <Tab>Application Overview</Tab>
                    <Tab>Personal Information</Tab>
                    <Tab>Drivers License</Tab>
                    <Tab>Vehicle Information</Tab>
                    <Tab>Banking Information</Tab>
                </TabList>

                <TabPanel className="appOverviewTab">
                    <AppOverview
	                    changeTab={this.changeTab}
                    />
                </TabPanel>

                <TabPanel className="personalInfoTab">
                    <PersonalInfo
	                    data={this.props.data}
	                    setPersonalInfo={this.props.setPersonalInfo}
                    />
                </TabPanel>

                <TabPanel className="driversLicenseTab">
                    <DriversLicense
	                    data={this.props.data}
	                    setLicense={this.props.setLicense}
                    />
                </TabPanel>

                <TabPanel className="vehicleInfoTab">
                    <VehicleInfo
	                    data={this.props.data}
	                    setVehicle={this.props.setVehicle}
                    />
                </TabPanel>

                <TabPanel className="bankingInfoTab">
                    <BankingInfo
	                    data={this.props.data}
	                    setBankInfo={this.props.setBankInfo}
                    />
                </TabPanel>
            </Tabs>
        )
    }
}

function mapStateToProps(state: StoreState) {
    return {
    	data: state.data,
        routing: state.routing
    }
}

function mapDispatchToProps(
	dispatch:
		Dispatch<PersonalInfoActions
			| LicenseActions
			| VehicleActions
			| BankInfoActions
			>) {
    return bindActionCreators(
        {
	        setPersonalInfo,
	        setLicense,
	        setVehicle,
	        setBankInfo
        },
        dispatch
    )
}

export const OverviewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Overview)

interface MockungOptions {
    [key: string]: Array<string>
}

export const mockingOptions: MockungOptions = {
    state: [
        'Arizona',
        'Alabama',
        'Alaska',
        'California',
        'Colorado',
        'Florida'
    ],
    carsBrands: [
        'Mercedes',
        'Aston Martin',
        'Audi',
        'BMW',
        'Bentley',
        'Bugatti'
    ],
    carsModels: [
        'Truck',
        'Sedan',
        'Hatchback',
        'Crossover',
        'Coupe'
    ],
    carsYears: [
        '2018',
        '2017',
        '2016',
        '2015',
        '2014',
        '2013',
        '2012',
        '2011',
        '2010'
    ],
	whoseHauler: [
	    'Individual Hauler',
        'Another 1',
        'Another 2',
        'Another 3'
    ],
	carsProfiles: [
        'Pickup truck only',
		'Another 1',
		'Another 2',
		'Another 3'
    ]
}

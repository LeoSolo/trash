export const HAULER_API = {
    AUTH_URL: 'https://gotrashy.auth.us-west-2.amazoncognito.com/',
    CLIENT_ID: '4phrpli06giehv5h9apjl4b60s',
    REDIRECT_URI_LOGIN: window.location.origin,
    REDIRECT_URI_LOGOUT: window.location.origin + '/Logout'
}

import { FirstFormActions } from '../actions/firstForm'
import { PersonalInfoActions } from '../actions/personalInfo'
import { LicenseActions } from '../actions/driversLicense'
import { VehicleActions } from '../actions/vehicle'
import { BankInfoActions } from '../actions/bankInfo'

import * as constants from '../constants/datas'

export interface DataState {
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    company: string,
    zipCode: string,
    birthday: any,
	userAvatar: string,
    hearAbout: string,
    ownTruck: boolean,
    ownTrailer: boolean,
    bgCheck: boolean,
	whoseHauler: string,
    street: string,
    city: string,
	userState: string,
	licenseState: string,
	licenseNumber: string,
	licenseExpires: string,
	licenseFrontImg: string,
	licenseBackImg: string,
	carBrand: string,
	carModel: string,
	carYear: string,
	carProfile: string,
	carPhoto1: string,
	carPhoto2: string,
	carPhoto3: string,
	carPhoto4: string,
	carPhoto5: string,
	bankInfoCard: string,
	bankRoutingNumber: string,
	bankAccountNumber: string,
	bankSocialSecurityNumber: string,
	debitCardInfo: string,
	expiredCardDate: string,
	cardCCVNumber: string
}

const initialState: DataState = {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    company: '',
    zipCode: '',
	birthday: '',
	userAvatar: '',
    hearAbout: '',
    ownTruck: false,
    ownTrailer: false,
    bgCheck: false,
	whoseHauler: '',
	street: '',
	city: '',
	userState: '',
	licenseState: '',
	licenseNumber: '',
	licenseExpires: '',
	licenseFrontImg: '',
	licenseBackImg: '',
	carBrand: '',
	carModel: '',
	carYear: '',
	carProfile: '',
	carPhoto1: '',
	carPhoto2: '',
	carPhoto3: '',
	carPhoto4: '',
	carPhoto5: '',
	bankInfoCard: '',
	bankRoutingNumber: '',
	bankAccountNumber: '',
	bankSocialSecurityNumber: '',
	debitCardInfo: '',
	expiredCardDate: '',
	cardCCVNumber: ''
}

type Actions = FirstFormActions
	& PersonalInfoActions
	& LicenseActions
	& VehicleActions
	& BankInfoActions

export function dataReducer(state: DataState = initialState, action: Actions): DataState {
	switch (action.type) {
        case constants.SAVE_FIRST_FORM: {
	        const {
		        firstName,
		        lastName,
		        email,
		        phone,
		        company,
		        zipCode,
		        hearAbout,
		        ownTruck,
		        ownTrailer,
		        bgCheck
	        } = action.payload
	        return {
		        ...state,
		        firstName,
		        lastName,
		        email,
		        phone,
		        company,
		        zipCode,
		        hearAbout,
		        ownTruck,
		        ownTrailer,
		        bgCheck
	        }
        }
        case constants.SAVE_PERSONAL_INFO: {
	        const {
		        firstName,
		        lastName,
		        email,
		        phone,
		        company,
		        whoseHauler,
		        street,
		        city,
		        userState,
                zipCode,
		        birthday,
		        userAvatar
	        } = action.payload
	        return {
		        ...state,
		        firstName,
		        lastName,
		        email,
		        phone,
		        company,
		        whoseHauler,
		        street,
		        city,
		        userState,
                zipCode,
		        birthday,
		        userAvatar
	        }
        }
		case constants.SAVE_DRIVER_LICENSE: {
			const {
				licenseState,
				licenseNumber,
				licenseExpires,
				licenseFrontImg,
				licenseBackImg
			} = action.payload
			return {
				...state,
				licenseState,
				licenseNumber,
				licenseExpires,
				licenseFrontImg,
				licenseBackImg
			}
		}
        case constants.SAVE_VEHICLE: {
            const {
	            carBrand,
	            carModel,
	            carYear,
	            carProfile,
	            carPhoto1,
	            carPhoto2,
	            carPhoto3,
	            carPhoto4,
	            carPhoto5
            } = action.payload
            return {
                ...state,
	            carBrand,
	            carModel,
	            carYear,
	            carProfile,
	            carPhoto1,
	            carPhoto2,
	            carPhoto3,
	            carPhoto4,
	            carPhoto5
            }
        }
		case constants.SAVE_BANK_INFO: {
        	const {
		        bankInfoCard,
		        bankRoutingNumber,
		        bankAccountNumber,
		        bankSocialSecurityNumber,
		        debitCardInfo,
		        expiredCardDate,
		        cardCCVNumber
	        } = action.payload
			return {
				...state,
				bankInfoCard,
				bankRoutingNumber,
				bankAccountNumber,
				bankSocialSecurityNumber,
				debitCardInfo,
				expiredCardDate,
				cardCCVNumber
			}
        }
	}
	return state
}

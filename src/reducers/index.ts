import { combineReducers } from 'redux'
import { routerReducer, RouterState } from 'react-router-redux'
import { DataState, dataReducer } from './data'
import { AccessTokenState, accessTokenReducer } from './accessToken'

export interface StoreState {
    routing: RouterState
    data: DataState
	accessToken: AccessTokenState
}

export const reducers = combineReducers<StoreState>({
    routing: routerReducer,
    data: dataReducer,
	accessToken: accessTokenReducer
})

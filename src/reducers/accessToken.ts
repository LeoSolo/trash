import { AccessTokenActions } from '../actions/accessToken'
import * as constants from '../constants/accessToken'

export interface AccessTokenState {
	accessToken: string
}

const initialState: AccessTokenState = {
	accessToken: ''
}

type Actions = AccessTokenActions

export function accessTokenReducer(state: AccessTokenState = initialState, action: Actions): AccessTokenState {
	switch (action.type) {
		case constants.SAVE_ACCESS_TOKEN: {
			const {
				accessToken
			} = action.payload
			return {
				accessToken
			}
		}
	}
	return state
}

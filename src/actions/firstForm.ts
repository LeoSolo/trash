import * as constants from '../constants/datas'

export interface SetFirstFormAction {
    type: string
    payload: {
        firstName: string,
        lastName: string,
        email: string,
        phone: string,
        company: string,
        zipCode: string,
        hearAbout: string,
        ownTruck: boolean,
        ownTrailer: boolean,
        bgCheck: boolean
    }
}

export type FirstFormActions = SetFirstFormAction

export function setFirstForm(
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    company: string,
    zipCode: string,
    hearAbout: string,
    ownTruck: boolean,
    ownTrailer: boolean,
    bgCheck: boolean
): SetFirstFormAction {
    return {
        type: constants.SAVE_FIRST_FORM,
        payload: {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            company: company,
            zipCode: zipCode,
            hearAbout: hearAbout,
            ownTruck: ownTruck,
            ownTrailer: ownTrailer,
            bgCheck: bgCheck
        }
    }
}

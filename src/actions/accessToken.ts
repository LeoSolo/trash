import * as constants from '../constants/accessToken'

export interface SetAccessToken {
	type: string,
	payload: {
		accessToken: string
	}
}

export type AccessTokenActions = SetAccessToken

export function setAccessToken(
	accessToken: string
): SetAccessToken {
	return {
		type: constants.SAVE_ACCESS_TOKEN,
		payload: {
			accessToken: accessToken
		}
	}
}

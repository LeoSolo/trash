import * as constants from '../constants/datas'

export interface SetLicenseAction {
	type: string
	payload: {
		licenseState: string,
		licenseNumber: string,
		licenseExpires: string,
		licenseFrontImg: string,
		licenseBackImg: string
	}
}

export type LicenseActions = SetLicenseAction

export function setLicense(
	licenseState: string,
	licenseNumber: string,
	licenseExpires: string,
	licenseFrontImg: string,
	licenseBackImg: string
): SetLicenseAction {
	return {
		type: constants.SAVE_DRIVER_LICENSE,
		payload: {
			licenseState: licenseState,
			licenseNumber: licenseNumber,
			licenseExpires: licenseExpires,
			licenseFrontImg: licenseFrontImg,
			licenseBackImg: licenseBackImg
		}
	}
}

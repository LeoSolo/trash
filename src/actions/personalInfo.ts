import * as constants from '../constants/datas'

export interface SetPersonalInfoAction {
	type: string
	payload: {
		company: string,
		whoseHauler: string,
		firstName: string,
		lastName: string,
		email: string,
		phone: string,
		street: string,
		city: string,
		userState: string,
		zipCode: string,
		birthday: any,
		userAvatar: string
	}
}

export type PersonalInfoActions = SetPersonalInfoAction

export function setPersonalInfo (
	company: string,
	whoseHauler: string,
	firstName: string,
	lastName: string,
	email: string,
	phone: string,
	street: string,
	city: string,
	userState: string,
	zipCode: string,
	birthday: any,
	userAvatar: string
): SetPersonalInfoAction {
	return {
		type: constants.SAVE_PERSONAL_INFO,
		payload: {
			company: company,
			whoseHauler: whoseHauler,
			firstName: firstName,
			lastName: lastName,
			email: email,
			phone: phone,
			street: street,
			city: city,
			userState: userState,
			zipCode: zipCode,
			birthday: birthday,
			userAvatar: userAvatar
		}
	}
}

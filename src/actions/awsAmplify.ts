import * as constants from '../constants/awsAmplify'

export interface AwsAuthSignInAction {
    type: string
}

export type AwsAmplifyActions = AwsAuthSignInAction

export function awsAuthSignIn(): AwsAuthSignInAction {
    return {
        type: constants.AWS_AMPLIFY_AUTH_SIGN_IN
    }
}

import * as constants from '../constants/datas'

export interface SetBankInfoAction {
	type: string,
	payload: {
		bankInfoCard: string,
		bankRoutingNumber: string,
		bankAccountNumber: string,
		bankSocialSecurityNumber: string,
		debitCardInfo: string,
		expiredCardDate: string,
		cardCCVNumber: string
	}
}

export type BankInfoActions = SetBankInfoAction

export function setBankInfo (
	bankInfoCard: string,
	bankRoutingNumber: string,
	bankAccountNumber: string,
	bankSocialSecurityNumber: string,
	debitCardInfo: string,
	expiredCardDate: string,
	cardCCVNumber: string
): SetBankInfoAction {
	return {
		type: constants.SAVE_BANK_INFO,
		payload: {
			bankInfoCard: bankInfoCard,
			bankRoutingNumber: bankRoutingNumber,
			bankAccountNumber: bankAccountNumber,
			bankSocialSecurityNumber: bankSocialSecurityNumber,
			debitCardInfo: debitCardInfo,
			expiredCardDate: expiredCardDate,
			cardCCVNumber: cardCCVNumber
		}
	}
}

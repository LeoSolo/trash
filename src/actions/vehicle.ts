import * as constants from '../constants/datas'

export interface SetVehicleAction {
	type: string
	payload: {
		carBrand: string,
		carModel: string,
		carYear: string,
		carProfile: string,
		carPhoto1: string,
		carPhoto2: string,
		carPhoto3: string,
		carPhoto4: string,
		carPhoto5: string
	}
}

export type VehicleActions = SetVehicleAction

export function setVehicle(
	carBrand: string,
	carModel: string,
	carYear: string,
	carProfile: string,
	carPhoto1: string,
	carPhoto2: string,
	carPhoto3: string,
	carPhoto4: string,
	carPhoto5: string
): SetVehicleAction {
	return {
		type: constants.SAVE_VEHICLE,
		payload: {
			carBrand: carBrand,
			carModel: carModel,
			carYear: carYear,
			carProfile: carProfile,
			carPhoto1: carPhoto1,
			carPhoto2: carPhoto2,
			carPhoto3: carPhoto3,
			carPhoto4: carPhoto4,
			carPhoto5: carPhoto5
		}
	}
}

interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: Function 
}

interface NodeModule {
    hot: {
        accept: Function
    }
}

interface EventTarget {
    dataset?: any,
	name?: any,
    value?: any,
	classList: any
}
